require 'epub/directory/version'
require "epub/parser"
require "pathname"
require "rss/opds"
require "haml"

module EPUB
  class Directory
    SRC_DIR = Pathname.new("src")
    DEST_DIR = Pathname.new("build")

    class << self
      def run(paths)
        paths.each do |path|
          add path
        end
        build
      end

      def add(path)
        EPUB::Parser.parse(path).renditions.each do |rendition|
          add_rendition rendition
        end
      end

      def build
        new.build
      end

      def add_rendition(rendition)
        Rendition.add rendition
      end
    end

    def build
      DEST_DIR.mkpath
      opfs = []
      Pathname.glob("#{SRC_DIR}/**/*.opf").each do |opf_path|
        make_destination_directory opf_path
        build_opf opf_path
        build_html opf_path
        build_opds opf_path

        opfs << opf_path.to_path.sub(SRC_DIR.to_path, DEST_DIR.to_path)
      rescue => error
        warn "Error occurred while processing #{opf_path}"
        raise if $DEBUG
      end
      build_indices opfs
    end

    def make_destination_directory(opf_path)
      dest_path = opf_path.to_path.sub(SRC_DIR.to_path, DEST_DIR.to_path)
      dest_path = Pathname.new(dest_path)
      dest_path.dirname.mkpath
    end

    def build_opf(opf_path)
      dest_path = opf_path.to_path.sub(SRC_DIR.to_path, DEST_DIR.to_path)
      dest_path = Pathname.new(dest_path)
      dest_path.write opf_path.read
    end

    def build_html(opf_path)
      dest_path = opf_path.sub_ext(".html").to_path.sub(SRC_DIR.to_path, DEST_DIR.to_path)
      dest_path = Pathname.new(dest_path)
      rendition = EPUB::Parser::Publication.new(opf_path.read).parse
      metadata = rendition.metadata
      page_title = "Information on \"#{metadata.title}\""
      template_path = File.join(__dir__, "../../templates/release-identifier.haml")
      engine = Haml::Engine.new(File.read(template_path))
      engine.options[:format] = :html5
      dest_path.write engine.render(binding)
    end

    def build_opds(opf_path)
      dest_path = opf_path.sub_ext(".opds").to_path.sub(SRC_DIR.to_path, DEST_DIR.to_path)
      dest_path = Pathname.new(dest_path)
      rendition = EPUB::Parser::Publication.new(opf_path.read).parse
      opds = RSS::Maker.make("atom:entry") {|maker|
        maker.items.new_item do |entry|
          entry.id = rendition.metadata.release_identifier
          entry.updated = rendition.metadata.modified.to_s
          entry.title = rendition.metadata.title
          # TODO: Filter authors by refines
          creators = rendition.metadata.creators + rendition.metadata.metas.select {|meta| ["dc:publisher", "dcterms:creator"].include? meta.property}
          creators.each do |creator|
            entry.authors.new_author do |author|
              author.name = creator.to_s
            end
          end
          entry.author = "N/A" unless entry.author
          entry.summary = rendition.metadata.description
          entry.links.new_link do |link|
            link.rel = RSS::OPDS::RELATIONS["acquisition"] # TODO: Arrange by info.toml
            link.href = dest_path.basename.sub_ext(".epub").to_path # TODO: Arrange by info.toml
            link.type = "application/epub+zip"
          end
        end
      }
      dest_path.write opds
    end

    def build_json(opf_path)
      raise NotImplementedError
    end

    def build_indices(opfs)
      dest_path = DEST_DIR/"index.html"
      page_title = "EPUB Directory"
      items = opfs.collect {|path|
        {
          "uri" => Pathname.new(path).sub_ext(".html").to_path.sub(DEST_DIR.to_path, ""),
          "metadata" => EPUB::Parser::Publication.new(File.read(path)).parse.metadata
        }
      }
      items.sort_by! {|item| item["metadata"].release_identifier}
      template_path = File.join(__dir__, "../../templates/index.haml")
      engine = Haml::Engine.new(File.read(template_path))
      engine.options[:format] = :html5
      dest_path.write engine.render(binding)
    end

    class Rendition
      class << self
        def add(rendition)
          new(rendition).add
        end
      end

      def initialize(rendition)
        @rendition = rendition
      end

      def add
        make_source_directory
        add_package_document
      end

      private

      def book
        @rendition.book
      end

      def source_directory
        SRC_DIR/@rendition.unique_identifier.to_s
      end

      def source_path
        source_directory/"#{@rendition.metadata.modified}.opf"
      end

      def make_source_directory
        source_directory.mkpath
      end

      def add_package_document
        package_xml = book.container_adapter.read(book.epub_file, @rendition.full_path.to_s)
        source_path.write package_xml
      end
    end
  end
end
