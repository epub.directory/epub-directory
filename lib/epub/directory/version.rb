#
# Copyright (c) 2018 KITAITI Makoto (KitaitiMakoto at gmail.com)
#
# epub-directory is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# epub-directory is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with epub-directory.  If not, see <http://www.gnu.org/licenses/>.
#

module EPUB
  class Directory
    # epub-directory version
    VERSION = "0.1.6"
  end
end
