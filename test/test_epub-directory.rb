require 'helper'
require 'epub/directory'
require "tmpdir"
require "pathname"

class TestEPUBDirectory < Test::Unit::TestCase

  def setup
    @epub_path = Pathname.new("test/fixtures/wasteland.epub").expand_path
    @opf_path = Pathname.new("test/fixtures/wasteland.opf").expand_path
    @source_path = Pathname.new("src/code.google.com.epub-samples.wasteland-basic/2012-01-18T12:47:00Z.opf")
  end

  def test_version
    version = EPUB::Directory.const_get('VERSION')

    assert !version.empty?, 'should have a VERSION constant'
  end

  def test_add
    Dir.mktmpdir do |dir|
      Dir.chdir dir do
        EPUB::Directory.add @epub_path
        assert_path_exist @source_path
        assert_equal @opf_path.read, @source_path.read
      end
    end
  end

  data(
    "opf" => [:build_opf, "build/code.google.com.epub-samples.wasteland-basic/2012-01-18T12:47:00Z.opf"],
    "html" => [:build_html, "build/code.google.com.epub-samples.wasteland-basic/2012-01-18T12:47:00Z.html"],
    "opds" => [:build_opds, "build/code.google.com.epub-samples.wasteland-basic/2012-01-18T12:47:00Z.opds"],
    "json" => [:build_json, "build/code.google.com.epub-samples.wasteland-basic/2012-01-18T12:47:00Z.json"],
  )
  def test_build_file(data)
    build_method, dest_path = data
    pend if build_method == :build_json
    dest_path = Pathname.new(dest_path)
    Dir.mktmpdir do |dir|
      Dir.chdir dir do
        EPUB::Directory.add @epub_path
        EPUB::Directory.new.make_destination_directory @source_path
        EPUB::Directory.new.send build_method, @source_path
        assert_path_exist dest_path
        assert_equal File.read(@opf_path.sub_ext(dest_path.extname)).chomp, dest_path.read.chomp
      end
    end
  end

end
