### 0.1.7 / Unreleased

* Sort items in index page

### 0.1.6 / 2020-02-09

* [BUG FIX]Don't overwrite author in OPDS when it exists
* Use book title as page title

### 0.1.5 / 2020-02-08

* Use variable for page title

### 0.1.4 / 2020-02-08

* Add index page template

### 0.1.3 / 2020-02-08

* Build index page

### 0.1.0 / 2018-10-21

* Initial release:

