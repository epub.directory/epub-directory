# -*- encoding: utf-8 -*-

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'epub/directory/version'

Gem::Specification.new do |gem|
  gem.name          = "epub-directory"
  gem.version       = EPUB::Directory::VERSION
  gem.summary       = %q{EPUB directory builder}
  gem.description   = %q{Builds EPUB directory static site from EPUB files, package documents or Web Publication manifests.}
  gem.license       = "AGPL-3.0"
  gem.authors       = ["KITAITI Makoto"]
  gem.email         = "KitaitiMakoto@gmail.com"
  gem.homepage      = "https://gitlab.com/epub.directory/epub-directory"

  gem.files         = `git ls-files`.split($/)

  `git submodule --quiet foreach --recursive pwd`.split($/).each do |submodule|
    submodule.sub!("#{Dir.pwd}/",'')

    Dir.chdir(submodule) do
      `git ls-files`.split($/).map do |subpath|
        gem.files << File.join(submodule,subpath)
      end
    end
  end
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ['lib']

  gem.add_runtime_dependency "epub-parser"
  gem.add_runtime_dependency "tomlrb"
  gem.add_runtime_dependency "haml"
  gem.add_runtime_dependency "rss-opds"

  gem.add_development_dependency 'bundler'
  gem.add_development_dependency 'rake'
  gem.add_development_dependency 'rubygems-tasks'
  gem.add_development_dependency 'yard'
  gem.add_development_dependency "test-unit"
  gem.add_development_dependency "test-unit-notify"
end
