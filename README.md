# EPUB Directory

* [Homepage](https://rubygems.org/gems/epub-directory)
* [Documentation](http://rubydoc.info/gems/epub-directory/frames)
* [Email](mailto:KitaitiMakoto at gmail.com)

## Description

Builds EPUB directory static site from EPUB files, package documents or Web Publication manifests.

## Features

* Registers EPUB information into `src` directory.
* Builds static EPUB directory site to `build` directory.

## Examples

    require 'epub/directory'

## Requirements

## Install

    $ gem install epub-directory

## Synopsis

### Registering EPUB info

    $ epub-directory path/to/document.epub

This command makes `src` directory, registers under it, and then build EPUB directory site.

### Build EPUB directory site

    $ epub-directory

This command builds EPUB directory site to `build` directory.

## Copyright


Copyright (c) 2018 Kitaiti Makoto

EPUB Directory is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

EPUB Directory is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with EPUB Directory.  If not, see <http://www.gnu.org/licenses/>.
